let userNumber;

while (!isNumberValid(userNumber)) {
    userNumber = prompt('Please, enter a number', [userNumber])
}

function isNumberValid(x) {
    if (typeof (+x) === "number" && !Number.isNaN(+x) && x?.length > 0 && x[0] !== " ") {
        return true;
    } else {
        return false;
    }
}

function getFactorial(num) {
    let factor = 1;
    for (let i = 1; i <= num; i++) {
        factor *= i;
    }
    return factor;
}

console.log(getFactorial(userNumber));